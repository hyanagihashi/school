package school;

public class SchoolMain {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		ClassRoom cr1=new ClassRoom("99JY01");
		cr1.setTeacher(new Teacher("やなぎ"));
		cr1.addStudent(new Student("99JY0101","電子太郎"));
		cr1.addStudent(new Student("99JY0102","電子花子"));
		System.out.println(cr1.getName());
		System.out.println(cr1.getTeacher().getName());
		for(Student s1 : cr1.getStudents()){
			System.out.println(s1.getId()+":"+s1.getName());
		}
	}
}
