package school;

import java.util.ArrayList;

public class ClassRoom {
	String name;
	Teacher teacher;
	ArrayList<Student> students= new ArrayList<>();
	public ClassRoom(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public ArrayList<Student> getStudents() {
		return students;
	}
	public void addStudent(Student st){
		students.add(st);
	}
}
